import { Component, OnInit, Input ,Output, EventEmitter } from '@angular/core';
import { AutoresService } from '../../services/autores.service';
import { destacado } from '../../interfaces/autores.interface';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: [
  ]
})
export class NavbarComponent implements OnInit {
  
  data!: destacado[];

  @Input() button: any;
  @Input() Is: boolean = false;
  @Output() Ischange = new EventEmitter<boolean>();

  constructor(private autoresService: AutoresService) {}


  ngOnInit() {
    this.autoresService.getDestacados()
      .subscribe({
        next: (res) => this.data = (res)
      });
  }

  change(){
    this.Is= !this.Is;
    this.Ischange.emit(this.Is);
  }
}
