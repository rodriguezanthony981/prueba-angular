import { Component, OnInit } from '@angular/core';
import { AutoresService } from '../../services/autores.service';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-autores-tabla',
  templateUrl: './autores-tabla.component.html',
  styles: [
  ]
})
export class AutoresTablaComponent implements OnInit{
  
  data!: string[];
  
  favorito!: string;
  name!: string;

  constructor(private autoresService: AutoresService, public dialog: MatDialog  ) {  }
  
  ngOnInit() { 
    this.autoresService.getAutores()
      .subscribe({
        next: ({authors}) => {
          return this.data = authors
        },
        error: (err) => console.log(err)
      })
    }
  
  openDialog(author:string): void {
    this.name = author;
    const dialogRef = this.dialog.open( ModalComponent, {
      data: { name: this.name, favorito: this.favorito },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.favorito = result;
      console.log(this.favorito);
    });
  }
}
