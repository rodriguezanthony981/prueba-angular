import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AutoresService } from '../../services/autores.service';
import { DialogData, obras } from '../../interfaces/autores.interface';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styles: [`
    .six{
      display: grid;
      width: 70vw;
      grid-template-columns: 60% 40%;
    }
  `
  ]
})
export class ModalComponent implements OnInit{
  
  obras!: obras[];
  obraS!: string;
  poemaContent: string = '';

  constructor(
    public dialogRef: MatDialogRef<ModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private autorService: AutoresService
  ) {}
  
  ngOnInit() {
    this.autorService.getObras(this.data.name)
      .subscribe({
        next: (res) => {
          console.log(res)
          return this.obras = res;
        }
      })
  } 

  onNoClick(): void {
    this.dialogRef.close();
  }

  buscarPoema(termino: string){
    this.autorService.getTitle(termino)
      .subscribe({
        next: res => {
          console.log(res);
          this.obraS = res[0].title;
          console.log(this.obraS)
          this.poemaContent = res[0].lines
          console.log(this.poemaContent)
          return this.poemaContent = res[0].lines;
        }
      })
  }
}
