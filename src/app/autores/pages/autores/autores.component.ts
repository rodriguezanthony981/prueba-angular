import { Component, OnInit } from '@angular/core';
import { AutoresService } from '../../services/autores.service';
import { destacado } from '../../interfaces/autores.interface';

@Component({
  selector: 'app-autores',
  templateUrl: './autores.component.html',
  styles: [`
    .grd{
      margin-top: 30px;
      display: grid;
      justify-content: center;
    }
    .thw{
      width: 260px;
    }
  `]
})
export class AutoresComponent implements OnInit {
  
  data!: destacado[];
  Is: boolean = false;

  constructor(private autoresService: AutoresService  ) {  }


  ngOnInit() {
    this.autoresService.getDestacados()
      .subscribe({
        next: (res) => this.data = (res)
      });
  }

  change(){
    this.Is= !this.Is;
  }
  onMiVariableChange(nuevoValor: boolean) {
    this.change();
  }
  favoritos(){
    
  }
}
