import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutoresComponent } from './pages/autores/autores.component';
import { AutoresTablaComponent } from './components/autores-tabla/autores-tabla.component';
import { MaterialModule } from '../material/material.module';
import { ModalComponent } from './components/modal/modal.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FavoritasComponent } from './pages/favoritas/favoritas.component';

@NgModule({
  declarations: [
    AutoresComponent,
    AutoresTablaComponent,
    ModalComponent,
    NavbarComponent,
    FavoritasComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports:[
    AutoresComponent,
    AutoresTablaComponent
  ]
})
export class AutoresModule { }
