export interface authors {
  authors: string[];
}

export interface author {
  title:     string;
  author:    string;
  lines:     string[];
  linecount: string;
}

export interface destacado {
  title:  string;
  author: string;
}

export interface DialogData {
  favorito: any;
  name: string;
}

export interface obras {
  title: string;
}

