import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { authors, destacado } from '../interfaces/autores.interface';

@Injectable({
  providedIn: 'root'
})
export class AutoresService {

  private urlBase = 'https://poetrydb.org'

  constructor( private http: HttpClient ) { }
  
  getAutores(): Observable<authors> {
    return this.http.get<authors>(`${this.urlBase}/author`);
  }

  getDestacados(): Observable<destacado[]> {
    return this.http.get<destacado[]>(`${this.urlBase}/random/10/author,title.json`);
  }

  getObras(autor: string): Observable<any> {
    return this.http.get(`${this.urlBase}/author/${autor}/title`);
  }

  getTitle(title: string): Observable<any> {
    return this.http.get(`${this.urlBase}/title/${title}`);
  }

  verificacionUser(){
    
  }
}
