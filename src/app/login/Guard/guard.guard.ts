import { CanMatchFn } from '@angular/router';
import { inject } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { tap } from 'rxjs';

export const Guard: CanMatchFn = (route, segments) => {
  const authService = inject(LoginService);
  const routed = inject(Router);
  
  return authService.verificaAuthentication()
          .pipe(
            tap( estaAutenticado => {
              if(!estaAutenticado){
                routed.navigate(['./auth/login']);
              }
             })
          );
};
