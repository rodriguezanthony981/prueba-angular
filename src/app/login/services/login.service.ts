import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  _auth!: any;

  constructor( private http: HttpClient ) { }

  verificaAuthentication(): Observable<boolean>{
    const token = localStorage.getItem('token');
    console.log(token)
    if (token && token.includes('admin')) {  
      return of(true);
    } else {
      console.log('Bloqueado por el Guard');
      return of(false)
    };
  }

  login() {
    console.log('entro++++++');
    localStorage.setItem('token', 'admin');
  }
  loggout():void{
    localStorage.clear()
  }
}
