import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './login/pages/pages.component';
import { AutoresComponent } from './autores/pages/autores/autores.component';
import { Guard } from './login/Guard/guard.guard';
import { FavoritasComponent } from './autores/pages/favoritas/favoritas.component';


const routes: Routes = [
  {
    path: '',
    component: PagesComponent
  },
  {
    path: 'autores',
    component: AutoresComponent,
    canMatch: [ Guard ],
  },
  {
    path: 'favoritos',
    component: FavoritasComponent,
    canMatch: [ Guard ],
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
